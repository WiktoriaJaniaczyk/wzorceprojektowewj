package composite;

import org.w3c.dom.ls.LSOutput;

public class Waiter {

    private MenuComponent allMenu;

    public Waiter(MenuComponent allMenu) {
        this.allMenu = allMenu;
    }

    public String print(){

        return allMenu.print();

    }

    public void printBreakfast(){
        findMenu("Breakfast menu");
    }

    public void printLunch(){
        findMenu("Lunch menu");
    }

    public void printBeverages(){
        findMenu("Beverages menu");
    }

    private void findMenu(String menuName){
        allMenu.createComponentStream()
                .filter(e -> e.getName().equalsIgnoreCase(menuName))
//                .forEach(e -> System.out.println(e.print()));
                .findFirst()
                .ifPresentOrElse(e -> System.out.println(e.print()) ,()-> System.out.println("Menu not found."));
    }

}
