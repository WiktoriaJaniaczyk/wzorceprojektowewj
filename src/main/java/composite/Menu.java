package composite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//klasa node, służy do przechowywania calego menu
//np. menu sniadaniowe, ktore bedzie mialo kolejne MenuItemy
public class Menu extends MenuComponent{

    private String name;
    private String description;
    //menuComponents
    private List<MenuComponent> menuComponents = new ArrayList<>();

    public Menu(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public void add(MenuComponent component) {
        menuComponents.add(component);
    }

    @Override
    public void remove(MenuComponent component) {
        menuComponents.remove(component);
    }


    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Stream<MenuComponent> createComponentStream() {
        return menuComponents.stream();
    }

    @Override
    public String print(){
//        System.out.println(name + "\n" + description + "\n" +  (isVegetarian() ? "(v)" : ""));

        return  "\n" + " ----- " +
                getName() + " ----- " + "\n" +
                getDescription() + "\n" +
//                "------------------------------------------" +
                menuComponents.stream().map(MenuComponent::print).collect(Collectors.joining());

    }
}



