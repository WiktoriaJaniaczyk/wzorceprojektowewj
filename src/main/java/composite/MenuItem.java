package composite;

public class MenuItem extends MenuComponent{

    private double price;
    private String name;
    private String description;
    private boolean vegetarian;

    public MenuItem(double price, String name, String description, boolean vegetarian) {
        this.price = price;
        this.name = name;
        this.description = description;
        this.vegetarian = vegetarian;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public boolean isVegetarian() {
        return vegetarian;
    }

    @Override
    public String  print() {
//        System.out.println(name + "\n" + description + "\n" + price + "\n" + (isVegetarian() ? "(v)" : ""));
        return  "\n" + " * " + name + " * " + "\n" +
                description + " " + (isVegetarian() ? "(v)" : "") + "\n" +
                price + "zł" + "\n"  ;
//                ".................................";
    }
}
