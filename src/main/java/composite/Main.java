package composite;

public class Main {

    public static void main(String[] args) {

        MenuComponent breakfastMenu = new Menu("Breakfast menu", "Sweet or savory breakfast");
        MenuComponent lunchMenu = new Menu("Lunch menu", "Small or big lunches");
        MenuComponent beveragesMenu = new Menu("Beverages menu", "Hot and cold beverages");
        MenuComponent allMenu = new Menu("Menu", "All menu");

        allMenu.add(breakfastMenu);
        allMenu.add(lunchMenu);
        allMenu.add(beveragesMenu);

        MenuComponent tea = new MenuItem(2.0, "Tea", "Cup of classical english tea", true);
        MenuComponent englishBreakfast = new MenuItem(20.0, "English breakfast", "Typical english breakfast", false);
        MenuComponent eggs = new MenuItem(6.0, "Eggs", "Boiled eggs", true);
        MenuComponent crossaint = new MenuItem(5.0, "Crossaint", "Crossaint with butter", true);
        MenuComponent pancakes = new MenuItem(8.0, "Pancakes", "Pancakes with chocolate", true);

        beveragesMenu.add(tea);
        breakfastMenu.add(englishBreakfast);
        breakfastMenu.add(eggs);
        breakfastMenu.add(crossaint);
        breakfastMenu.add(pancakes);


        Waiter waiter = new Waiter(allMenu);

//        System.out.println(waiter.print());
        waiter.printBreakfast();
//        waiter.printBeverages();
//        waiter.printLunch();
        //
    }
}
