package abstractFactory;

public class DarkLabel extends Label{
    @Override
    public void happyLabel() {
        System.out.println("Dark label");
    }
}
