package abstractFactory;

public class WhiteLabel extends Label{
    @Override
    public void happyLabel() {
        System.out.println("White label");
    }
}
