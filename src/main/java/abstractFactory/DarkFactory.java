package abstractFactory;

public class DarkFactory extends GuiFactory{


    @Override
    Button createButton() {
        return new DarkButton();
    }

    @Override
    Label createLabel() {
        return new DarkLabel();
    }

    @Override
    TextBox createTextBox() {
        return new DarkTextBox();
    }
}
