package abstractFactory;

public abstract class GuiFactory {

    abstract Button createButton();
    abstract Label createLabel();
    abstract TextBox createTextBox();

}
