package abstractFactory;

public class WhiteFactory extends GuiFactory{

    @Override
    Button createButton() {
        return new WhiteButton();
    }

    @Override
    Label createLabel() {
        return new WhiteLabel();
    }

    @Override
    TextBox createTextBox() {
        return new WhiteTextBox();
    }
}
