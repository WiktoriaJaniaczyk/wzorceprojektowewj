package abstractFactory;

public class WhiteTextBox extends TextBox{
    @Override
    public void happyTextBox() {
        System.out.println("White text box");
    }
}
