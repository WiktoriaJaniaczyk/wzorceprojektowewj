package abstractFactory;

public class DarkButton extends Button {


    @Override
    public void happyButton() {
        System.out.println("Dark button");
    }
}
