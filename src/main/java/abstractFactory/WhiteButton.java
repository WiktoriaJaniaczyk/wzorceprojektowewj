package abstractFactory;

public class WhiteButton extends Button{
    @Override
    public void happyButton() {
        System.out.println("White button");
    }
}
