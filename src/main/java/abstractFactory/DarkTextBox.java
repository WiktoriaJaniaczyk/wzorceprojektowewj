package abstractFactory;

public class DarkTextBox extends TextBox{
    @Override
    public void happyTextBox() {
        System.out.println("Dark text box");
    }
}
