package abstractFactory;

public class Main {

    public static void main(String[] args) {

         GuiFactory guiFactoryWhite = new WhiteFactory();
         guiFactoryWhite.createButton().happyButton();
         guiFactoryWhite.createLabel().happyLabel();
         guiFactoryWhite.createTextBox().happyTextBox();

        GuiFactory guiFactoryDark = new DarkFactory();
        guiFactoryDark.createButton().happyButton();
        guiFactoryDark.createLabel().happyLabel();
        guiFactoryDark.createTextBox().happyTextBox();
    }
}
