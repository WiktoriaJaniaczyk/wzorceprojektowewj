package abstractFactory;

public abstract class Button {

    public abstract void happyButton();
}
