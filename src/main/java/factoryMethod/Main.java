package factoryMethod;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        AnimalFactory animalFactory = new RandomFactory();
        animalFactory.createAnimal().makeSound();
        AnimalFactory  b = new TypeFactory();
        b.createAnimal(TypeFactory.AnimalType.DOG).makeSound();
        AnimalFactory animalFactory2 = new RandomFactory();
        animalFactory2.createAnimal().makeSound();

//        int i = new Random().nextInt(3);
//        int j = new Random().nextInt(3);
//        int k = new Random().nextInt(3);
//        int l = new Random().nextInt(3);
//        int m = new Random().nextInt(3);
//        int n = new Random().nextInt(3);
//        System.out.println(i);
//        System.out.println(j);
//        System.out.println(k);
//        System.out.println(l);
//        System.out.println(m);
//        System.out.println(n);


    }
}
