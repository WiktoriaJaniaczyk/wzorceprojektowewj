package factoryMethod;

import java.util.Random;

public class RandomFactory extends AnimalFactory{
    @Override
    public Animal createAnimal() {
        int animalRandom = new Random().nextInt(3);
        switch(animalRandom){
            case 0:
                return  new Bird();
            case 1:
                return new Cat();
            case 2:
                return new Dog();
        }
        return null;
    }

}
