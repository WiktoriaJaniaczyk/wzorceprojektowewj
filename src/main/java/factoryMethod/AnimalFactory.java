package factoryMethod;

public abstract class AnimalFactory {

    public Animal createAnimal(){
        throw new UnsupportedOperationException();
    }

    public Animal createAnimal(TypeFactory.AnimalType animalType) {
        throw new UnsupportedOperationException();
    }
}
