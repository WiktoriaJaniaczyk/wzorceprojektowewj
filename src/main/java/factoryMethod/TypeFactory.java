package factoryMethod;

public class TypeFactory extends AnimalFactory{

    public enum AnimalType{
        DOG,
        CAT,
        BIRD;
    }

    @Override
    public Animal createAnimal(AnimalType animalType) {
        switch (animalType){
            case DOG:
                return new Dog();
            case CAT:
                return new Cat();
            case BIRD:
                return new Bird();
        }
        throw new IllegalArgumentException("The animal type " + animalType + " is not recognised.");
    }
}
