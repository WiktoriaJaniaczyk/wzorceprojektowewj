package builder.inteface;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Computer computer = new ComputerBuilderImpl()
                .ghz(2.7)
                .ram(2048)
                .screenInches(15.4)
                .productionDate(LocalDate.of(2016,12,2))
                .weight(1.9)
                .operatingSystem("macOS Catalina")
                .build();

        System.out.println(computer);

    }
}
