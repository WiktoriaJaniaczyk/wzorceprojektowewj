package builder.inteface;

import java.time.LocalDate;

public interface ComputerBuilder {

    Computer build();

    ComputerBuilder ghz(double ghz);
    ComputerBuilder ram(int ram);
    ComputerBuilder screenInches (double screenInches);
    ComputerBuilder productionDate (LocalDate productionDate);
    ComputerBuilder weight (double weight);
    ComputerBuilder operatingSystem (String operatingSystem);

    //zawsze zwracamy this - ComputerBuilder
    //wtedy możemy korzystać z fluent buildera
    // .ghz
    // .ram
    // .weight etc...
}
