package builder.inteface;

import java.time.LocalDate;

public class ComputerBuilderImpl implements ComputerBuilder {
    private Computer computer;

    public ComputerBuilderImpl() {
        this.computer = new Computer();
    }

    @Override
    public Computer build() {
        return computer;
    }

    @Override
    public ComputerBuilder ghz(double ghz) {
        computer.setGhz(ghz);
        return this;
    }

    @Override
    public ComputerBuilder ram(int ram) {
        computer.setRam(ram);
        return this;
    }

    @Override
    public ComputerBuilder screenInches(double screenInches) {
        computer.setScreenInches(screenInches);
        return this;
    }

    @Override
    public ComputerBuilder productionDate(LocalDate productionDate) {
        computer.setProductionDate(productionDate);
        return this;
    }

    @Override
    public ComputerBuilder weight(double weight) {
        computer.setWeight(weight);
        return this;
    }

    @Override
    public ComputerBuilder operatingSystem(String operatingSystem) {
        computer.setOperatingSystem(operatingSystem);
        return this;
    }
}
