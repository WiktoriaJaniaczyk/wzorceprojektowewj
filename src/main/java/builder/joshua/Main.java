package builder.joshua;

public class Main {

    public static void main(String[] args) {
        Computer computer = new Computer.Builder()
                .ghz(2.4)
                .ram(2048)
                .build();

        System.out.println(computer);
    }
}
