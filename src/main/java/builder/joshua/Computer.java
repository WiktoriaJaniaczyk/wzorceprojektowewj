package builder.joshua;

import java.time.LocalDate;

public class Computer {

    private double ghz;
    private int ram;
    private double screenInches;
    private LocalDate productionDate;
    private double weight;
    private String operatingSystem;

    //jest immutable, czyli niezmienny ->  tylko przez builder można stworzyć obiekt
    private Computer(Builder builder) {
        this.ghz = builder.ghz;
        this.ram = builder.ram;
        this.screenInches = builder.screenInches;
        this.productionDate = builder.productionDate;
        this.weight = builder.weight;
        this.operatingSystem = builder.operatingSystem;
    }

    public double getGhz() {
        return ghz;
    }

    public int getRam() {
        return ram;
    }

    public double getScreenInches() {
        return screenInches;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public double getWeight() {
        return weight;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "ghz=" + ghz + '\'' +
                ", ram=" + ram + '\'' +
                ", screenInches=" + screenInches + '\'' +
                ", productionDate=" + productionDate + '\'' +
                ", weight=" + weight + '\'' +
                ", operatingSystem='" + operatingSystem + '\'' +
                '}';
    }

    public static class Builder {
        private double ghz;
        private int ram;
        private double screenInches;
        private LocalDate productionDate;
        private double weight;
        private String operatingSystem;

//        public Builder(double ghz) {
//            this.ghz = ghz;
//        }

        public Builder ghz(double ghz) {
            this.ghz = ghz;
            return this;
        }

        //command+n -> setter -> template  -> Builder

        public Builder ram(int ram) {
            this.ram = ram;
            return this;
        }

        public Builder screenInches(double screenInches) {
            this.screenInches = screenInches;
            return this;
        }

        public Builder productionDate(LocalDate productionDate) {
            this.productionDate = productionDate;
            return this;
        }

        public Builder weight(double weight) {
            this.weight = weight;
            return this;
        }

        public Builder operatingSystem(String operatingSystem) {
            this.operatingSystem = operatingSystem;
            return this;
        }

        public Computer build(){
            return new Computer(this);
        }
    }
}
