package decorator;

public class Main {

    public static void main(String[] args) {
//musimy miec zgodny typ, dlatego wszystko dziedziczy
        //dzieki dziedziczeniu poprzez polimorfizm
        Beverage espressoWithCaramelAndMilk = new CaramelDecorator(new MilkDecorator(new Espresso()));
        Beverage teaWithSugar = new SugarDecorator(new Tea());

        System.out.println(espressoWithCaramelAndMilk.getCost());
        System.out.println(espressoWithCaramelAndMilk.getDescription());
        System.out.println(teaWithSugar.getCost());
        System.out.println(teaWithSugar.getDescription());



    }
}
