package decorator;

//moze to byc zwykla klasa, abstrakcyjna lub interfejs
public abstract class Beverage {

    public String getDescription(){
        return "Unknown description";
//        throw new UnsupportedOperationException();
    }
//metoda abstrakcyjna
    public abstract double getCost();

}
