package decorator;

public class Tea extends Beverage {

    @Override
    public String getDescription() {
        return "Tea";
    }

    @Override
    public double getCost() {
        return 3.5;
    }
}
