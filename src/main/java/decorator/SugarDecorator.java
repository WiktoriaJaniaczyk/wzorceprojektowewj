package decorator;

public class SugarDecorator extends BeverageDecorator {

    public static final double SUGAR_PRICE = 0.5;

    public SugarDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double getCost() {
        double costOfBeverage = super.getCost();
        return costOfBeverage + SUGAR_PRICE;
    }

    @Override
    public String getDescription() {
        String descriptionOfBeverage = super.getDescription();
        return descriptionOfBeverage + " with sugar";
    }

}
