package decorator;

public class Americano extends Beverage {

    @Override
    public String getDescription() {
        return "Americano";
    }

    @Override
    public double getCost() {
        return 6.00;
    }
}
