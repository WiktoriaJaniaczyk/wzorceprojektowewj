package decorator;

public class CaramelDecorator extends BeverageDecorator{

    public static final double CARAMEL_PRICE = 1.5;

    public CaramelDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double getCost() {
        double costOfBeverage = super.getCost();
        return costOfBeverage + CARAMEL_PRICE;
    }

    @Override
    public String getDescription() {
        String descriptionOfBeverage = super.getDescription();
        return descriptionOfBeverage + " with caramel";
    }


}
