package strategy;

public class CreditCard implements PaymentStrategy {

    @Override
    public void payMethod() {
        System.out.println("Płatność kartą");

    }
}
