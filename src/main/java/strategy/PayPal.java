package strategy;

public class PayPal implements PaymentStrategy {

    @Override
    public void payMethod() {
        System.out.println("Płatność Paypal");

    }
}
