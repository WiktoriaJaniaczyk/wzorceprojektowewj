package strategy;

public class Main {

    public static void main(String[] args) {
        CreditCard creditCard = new CreditCard();
        PayPal payPal = new PayPal();
        Transfer transfer = new Transfer();

        OnlineShop onlineShop = new OnlineShop(creditCard);

        onlineShop.paymentMethod();
        onlineShop.setPaymentStrategy(payPal);
        onlineShop.paymentMethod();

    }
}
