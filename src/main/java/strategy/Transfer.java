package strategy;

public class Transfer implements PaymentStrategy {

    @Override
    public void payMethod() {
        System.out.println("Płatność przelewem");

    }
}
