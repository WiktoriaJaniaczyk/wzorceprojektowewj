package strategy;

public interface PaymentStrategy {
    void payMethod ();
}
