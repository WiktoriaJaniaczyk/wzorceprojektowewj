package singleton;

public class Main {

        public static void main(String[] args) {
            Singleton singleton = Singleton.getInstance();
            System.out.println(singleton.hashCode());
            singleton.incrementValue();
            System.out.println(singleton.getValue());

//            singleton = Singleton.getInstance();
            Singleton singleton1 = Singleton.getInstance();
            System.out.println(singleton1.hashCode());
            singleton.incrementValue();
            Singleton singleton2 = Singleton.getInstance();
            System.out.println(singleton2.hashCode());
            singleton2.incrementValue();
            System.out.println("S: " + singleton.getValue());
            System.out.println("S1: " +singleton1.getValue());
            System.out.println("S2: " +singleton2.getValue());
            System.out.println(singleton.toString());
            System.out.println(singleton1.toString());
            System.out.println(singleton2.toString());
        }

}
