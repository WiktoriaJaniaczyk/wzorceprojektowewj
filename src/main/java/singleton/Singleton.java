package singleton;

public class Singleton {

    private static Singleton INSTANCE;
    private int value = 0;

    private Singleton() {}

    public static Singleton getInstance() {
        if (INSTANCE == null) {
            synchronized (Singleton.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Singleton();
                }
            }
        }
        return INSTANCE;
    }

    public void incrementValue() {
        value++;
    }

    public int getValue() {
        return value;
    }
}
